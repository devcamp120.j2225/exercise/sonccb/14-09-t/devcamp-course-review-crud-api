// Import bộ thư viện express
const express = require('express'); 

// Import Course Middleware
const { printCourseURLMiddleware } = require("../middlewares/courseMiddleware");

// Import Course Controller
const { createCourse, getAllCourse, getCourseById, updateCourseById, deleteCourseById } = require("../controllers/courseController");

const router = express.Router();

router.get("/courses", printCourseURLMiddleware, getAllCourse);

router.post("/courses", printCourseURLMiddleware, createCourse);

router.get("/courses/:courseId", printCourseURLMiddleware, getCourseById);

router.put("/courses/:courseId", printCourseURLMiddleware, updateCourseById);

router.delete("/courses/:courseId", printCourseURLMiddleware, deleteCourseById);

// Export dữ liệu thành 1 module
module.exports = router;